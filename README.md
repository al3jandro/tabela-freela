# Projeto Tabela

## Front-end: componentes em Angular - Telas, controles e acesso a APIs

Projeto desenvolvido no Angular 8. Duas bibliotecas foram usadas neste projeto:

1. MdBootstrap => https://mdbootstrap.com/docs/angular/
2. sweetalert2 => https://sweetalert2.github.io/

Na estrutura do código, isso foi feito com **Lazy Loading**, além disso, foram criados **services** para chamar um banco de dados, usando o mesmo princípio para obter os dados da API.
O arquivo json está na seguente rota **assets**

Para instalar, depois do clonar projeto, solo execute `npm install`.
