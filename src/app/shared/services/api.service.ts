import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getJson(file: string): Observable<any> {
    return this.http.get<any>(`./assets/${ file }.json`);
  }

}
