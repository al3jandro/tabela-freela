import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  json: any = [];
  rows: any = [];
  heads = ['id', 'nome', 'descrição'];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getJson('db').subscribe(
      data => {
        this.json = data[0].table;
        this.rows = data[0].table.linha;
      }, err => {
        console.log(err);
      }
    );
  }

  dados(e: any, title: string) {
    Swal.fire({
      title:`Mensagem ${title}`,
      icon: 'info',
      html: `
        <ul>
          <li><strong>ID: </strong> ${e.id}</li>
          <li><strong>Nome: </strong> ${e.name}</li>
          <li><strong>Descrição: </strong> ${e.description}</li>
        </ul>
      `
    });
  }
}
